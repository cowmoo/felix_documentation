# Installing 3ASD "Automated assembly, annotation and signature detection"

The 3ASD Pipeline ((provisional internal name-- may change upon release)) has an 
automated installer that will take you through all  of the steps to installing the 
pipeline on any Linux/UNIX-based scientific environment. However, there's several 3rd
dependencies you might need to install. The following section of the document will
walk you through all of these steps, if necessary to install these dependencies.

## Install system dependencies

You must first install or ensure certain utilities programs are present on your host.

On Debian-based systems, including Ubuntu:

```sh
# Ensure repositories are up-to-date
sudo apt-get update
# Install debian packages for dependencies
sudo apt-get install -y \
    mailutils \
    jq \
    bsdmainutils \
    curl
```

On CentOS/RHEL:

```sh
# Install RPM packages for dependencies, verify
sudo yum install -y \
    mailutils \
    jq \
    bsdmainutils \
    curl
```

## Install Java 11

Broad FELIX Pipeline utilizes Java 11 as the pipeline GUI is built on this environment.
Newer or older version of JRE might be incompatible with this environment. 

On Debian-based systems, including Ubuntu: 
```sh 
sudo add-apt-repository ppa:openjdk-r/ppa
sudo apt-get update
sudo apt install openjdk-11-jdk
```

On CentOS/RHEL (Follow instructions here: https://phoenixnap.com/kb/install-java-on-centos)

Then run the following command and pick the `openjdk-11-jdk` from the list

```sh
update-alternatives --config java
```


## Install/Update Python 3 and Python Dependencies

Broad FELIX Pipeline file server is written in Python 3 and utilizes the `flask` package. 

If Python 3.8 (or newer) is not on your system:

On Debian-based systems, including Ubuntu: 
```sh 
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt update
sudo apt install python3.8
```

On CentOS/RHEL: (Follow instructions here: https://computingforgeeks.com/how-to-install-python-3-on-centos/)

Then install the following Python 3 packages using `pip`:
```sh
pip3 install flask flask_restx requests
```


## Install Singularity

Broad FELIX Pipeline make use of Singularity 3.8 as the containerization platform
to run all of its pipeline tasks. To install Singularity, please refer to 
[INSTALL_SINGULARITY.md](INTALL_SINGULARITY.md) or https://github.com/apptainer/singularity/blob/master/INSTALL.md 


## Running 3ASD Main Installer

Now we are ready to proceed with the main installation script

```sh
python3 main.py
```

The installation script involves a step-by-step series of questions about where to install 
the pipeline, the hostname of the server; and finally the locations of third party databases
if they exist already on the host or need to be downloaded on the fly. 

```sh
# Example installation answers: 

Enter name of target installation directory to create: /media/storage/paulcao/test_install3
Enter path to Java 11 $JAVA_HOME: /media/storage/paulcao/jdk-11.0.13/
Enter the server IP number or server hostname: saffron.mit.edu
Enter an existing nt path? [Or leave blank to download from source]:/media/storage/DK/blastdb_v5/
Enter an existing nr path? [Or leave blank to download from source]:/media/storage/paulcao/nr
Enter an existing SwissProt path? [Or leave blank to download from source]:/media/storage/paulcao/swissprot
Enter an existing univec path? [Or leave blank to download from source]: /media/storage/paulcao/test_install2/database/univec_db
Enter an existing TIGRfam path? [Or leave blank to download from source; or enter an existing path]:/media/storage/paulcao/test_install2/database/tigrfam_db
Enter an existing Pfam path? [Or leave blank to download from source]:/media/storage/paulcao/test_install2/database/pfam_db
Enter an existing NCBI Taxonomy database path? [Or leave blank to download from source]:/media/storage/paulcao/test_install2/database/taxdb
Enter an existing NCBI Blacklist path? [Or leave blank to download from source]:/media/storage/paulcao/test_install2/database/blacklist
```

Note for all BLAST databases (nt, nr, univec); one simply needs to point to the root directory of 
where the databases live (e.g., for nt; it's the directory that contains `nt.ndb` and other associated nt files). 

Downloading large databases such as nt or nr will take awhile. However if nt and nr are already present, 
downloading the other databases from source should take ~20-45 minutes. 

## After Installation

Once everything is complete. The installation script should prompt you with a command to launch 
the pipeline GUI. You can refer to the [QUICKSTART.md](QUICKSTART.md) to run a test job there.

```sh
sh $INSTALL_DIR/run_felix.sh
```
