# Quick Start

This guide will introduce the user how to run a simple virus sample through the
pipeline; from logging in, to submitting a job, monitoring and downloading files
after the pipeline is complete. 

## 1. Start Broad FELIX

First log onto your pipeline manager at `host:4440` and enter the default authentication
of `login\login`; then select the `FELIX` project and finally select `Complete Analysis Pipeline`.

<img src="/screenshots/login.png" width=350 />


## 2. Launch a New Job

On the screen below, you can input the set of your Illumina reads and Nanopore read for 
your assembly. e.g., you can input in: `$INSTALL_DIR\test_sample\B331.illumina.1.fq.gz`
and `$INSTALL_DIR\test_sample\B331.illumina.2.fq.gz` into the Illumina read fields and 
click `Run Job Now`. 

<img src="/screenshots/submit_job.png" width=450 />

## 3. Monitoring the Job

When the job is running, you should be able to see the status of the sub-tasks of the job
as they are queued up, ran and completed - all the way to the last task of the workflow
(if succeeded); or the last failed step. 

<img src="/screenshots/status.png" width=450 />


## Download the Pipeline

If the pipeline has finished successfully. There should be a second accordion called
`Pipeline Work Products & Report` which you can expand and be able to download or view in-line
work products of the assembly (the assembly, the GFF file, the final report etc).

<img src="/screenshots/work_results.png" width=450 />


